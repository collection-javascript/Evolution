﻿ module Evolution {

     export class Process {

         public Frame: number;
         public Interval: number;

         private _loop: number;
         private _lastTime: number;

         constructor() {
             this.Frame = 0;
             this.Interval = 25; // Milliseconds
         }

         public Start() {
             this._loop = setTimeout(() => this.Loop(), 0);
             this._lastTime = Date.now();
         }

         public Stop() {
             clearTimeout(this._loop);
         }

         public Loop() {
             this.Frame++;
             this._loop = setTimeout(() => this.Loop(), this.getDelay());
             this.Step();
         }

         private getDelay() {
             var thisTime = Date.now();
             var deltaTime = thisTime - this._lastTime;
             var delay = Math.max(this.Interval - deltaTime, 0);
             this._lastTime = thisTime + delay;
             return delay;
         }

         public Step() {
             // Override me. :D
             throw new Error('This method is abstract');
         }
     }
 }