﻿module Evolution {

    export class Environment {

        public Size: DataTypes.Vector;
        public Color: DataTypes.Color;

        constructor() {
            this.Size = new DataTypes.Vector(512, 512);
            this.Color = DataTypes.Color.RandomColor();
        }
    }
}