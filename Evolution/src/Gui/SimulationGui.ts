/// <reference path="../Process.ts" />
module Evolution.Gui {

    export class SimulationGui extends Process {

        private _simulation: Simulation;
        private _canvas: HTMLCanvasElement;
        private _context: CanvasRenderingContext2D;

        constructor(simulation: Simulation, canvas: HTMLCanvasElement) {
            super();
            this._simulation = simulation;
            this._canvas = canvas;
            this._context = this._canvas.getContext('2d');
        }

        public Step() {
            DrawHelper.ClearCanvas(this._context, this._canvas);
            this.drawEnvironment(this._simulation.Environment);
            this.drawSimulationObjects(this._simulation.SimulationObjects);
            this.Frame++;
        }

        private drawEnvironment(environment: Environment) {
            DrawHelper.FillCanvas(this._context, this._canvas, environment.Color);
        }

        private drawSimulationObjects(objects: SimulationObject[]) {
            for (var i = 0; i < objects.length; i++)
                this.drawSimulationObject(objects[i]);
        }

        private drawSimulationObject(object: SimulationObject) {
            DrawHelper.DrawNSide(this._context, object.Transform, 5, new DataTypes.Color(200, 200, 0, 1));
        }

    }

}