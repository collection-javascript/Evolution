﻿module Evolution.Gui {

    export class DrawHelper {

        static FillCanvas(ctx: CanvasRenderingContext2D, canvas: HTMLCanvasElement, color: DataTypes.Color) {
            ctx.fillStyle = color.GetRgba();
            ctx.fillRect(0, 0, canvas.width, canvas.height);
        }

        static ClearCanvas(ctx: CanvasRenderingContext2D, canvas: HTMLCanvasElement) {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
        }

        static DrawNSide(ctx: CanvasRenderingContext2D, transform: DataTypes.Transform, sides: number, color: DataTypes.Color) {
            ctx.beginPath();
            DrawHelper.DrawPolygon(ctx, transform, sides);
            ctx.fillStyle = color.GetRgba();
            ctx.fill();
            ctx.strokeStyle = new DataTypes.Color(255, 255, 255, 1).GetRgba();
            ctx.lineWidth = 4;
            ctx.stroke();
        }

        static DrawPolygon(ctx: CanvasRenderingContext2D, transform: DataTypes.Transform, sides: number) {
            if (sides < 3) return;
            var a = (Math.PI * 2) / sides;
            ctx.save();
            ctx.translate(transform.Position.x, transform.Position.y);
            ctx.rotate(transform.Rotation);
            ctx.moveTo(transform.Scale, 0);
            for (var i = 1; i < sides; i++) {
                ctx.lineTo(
                    transform.Scale * Math.cos(a * i),
                    transform.Scale * Math.sin(a * i));
            }
            ctx.closePath();
            ctx.restore();
        }
    }
}