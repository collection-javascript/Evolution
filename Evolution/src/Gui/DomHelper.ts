/// <reference path="../DataTypes/Vector.ts" />
module Evolution.Gui {

    export class DomHelper {

        static AppendElement(element: HTMLElement, parent: HTMLElement){
            parent.appendChild(element);
        }

        static NewWrapperElement() {
            var wrapper = document.createElement("div");
            wrapper.className = "wrapper";
            return wrapper;
        }

        static NewContainerElement() {
            var container = document.createElement("div");
            container.className = "container";
            return container;
        }

        static NewButtonElement(text: string, action: Function) {
            var button = document.createElement("button");
            button.addEventListener("click", () => action());
            button.innerHTML = text;
            return button;
        }

        static NewCanvasElement(size: DataTypes.Vector) {
            var canvas = document.createElement("canvas");
            canvas.width = size.x;
            canvas.height = size.y;
            return canvas;
        }
    }
}