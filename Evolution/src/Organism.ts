﻿/// <reference path="SimulationObject.ts" />
module Evolution {

    export class Organism extends SimulationObject {

        public Cells: Cell[];

        constructor() {
            super();
            this.Cells = [];
            this.Transform.Position = new DataTypes.Vector(MathHelper.RandomRange(0, 512, 0), MathHelper.RandomRange(0, 512, 0));
        }

        public Step() {
            
        }

        public AddCell() {
            var cell = new Cell();
            if (this.Cells.length != 0)
                cell.ConnectToRandomCell(this.Cells);
            this.Cells.push(cell);
            return cell;
        }
    }
}