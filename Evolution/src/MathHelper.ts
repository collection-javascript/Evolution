﻿module Evolution {

    export class MathHelper {

        static RandomRange(min: number, max: number, decimals: number) {
            var random = min + Math.random() * (max - min);
            var power = Math.pow(10, decimals);
            return Math.round(random * power) / power;
        }

        static LinearInterpolation(point1: number, point2: number, amount: number) {
            return point1 + (point2 - point1) * amount;
        }
    }
}