﻿module Evolution {

    export class Setup {

        public Simulation: Simulation;
        public SimulationGui: Gui.SimulationGui;

        private _wrapper;
        private _simulationCanvasBox;
        private _simulationCanvas;
        private _buttonBox;
        private _btnReroll;
        private _btnStopSimulation;
        private _btnStartSimulation;

        constructor() {
            this.buildElements();
            this.appendElements();
            this.Reroll();
        }

        private buildElements() {
            this._wrapper = Gui.DomHelper.NewWrapperElement();
            this._simulationCanvasBox = Gui.DomHelper.NewContainerElement();
            this._simulationCanvas = Gui.DomHelper.NewCanvasElement(new DataTypes.Vector(512, 512));
            this._buttonBox = Gui.DomHelper.NewContainerElement();
            this._btnReroll = Gui.DomHelper.NewButtonElement("Reroll", () => this.Reroll());
            this._btnStopSimulation = Gui.DomHelper.NewButtonElement("Start", () => { return null; });
            this._btnStartSimulation = Gui.DomHelper.NewButtonElement("Stop", () => { return null; });
        }

        private appendElements() {
            Gui.DomHelper.AppendElement(this._wrapper, document.body);
            Gui.DomHelper.AppendElement(this._simulationCanvasBox, this._wrapper);
            Gui.DomHelper.AppendElement(this._simulationCanvas, this._simulationCanvasBox);
            Gui.DomHelper.AppendElement(this._buttonBox, this._wrapper);
            Gui.DomHelper.AppendElement(this._btnReroll, this._buttonBox);
            Gui.DomHelper.AppendElement(this._btnStopSimulation, this._buttonBox);
            Gui.DomHelper.AppendElement(this._btnStartSimulation, this._buttonBox);
        }

        public Reroll() {
            if (this.Simulation)
                this.Simulation.Stop();

            if (this.SimulationGui)
                this.SimulationGui.Stop();

            this.Simulation = new Evolution.Simulation();
            this.Simulation.Start();

            this.SimulationGui = new Evolution.Gui.SimulationGui(this.Simulation, this._simulationCanvas);
            this.SimulationGui.Start();


            this.Simulation.AddSimulationObject(new Organism());
            this.Simulation.AddSimulationObject(new Organism());
            this.Simulation.AddSimulationObject(new Organism());
            this.Simulation.AddSimulationObject(new Organism());
        }  
    }
 }