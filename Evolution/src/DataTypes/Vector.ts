﻿/// <reference path="../MathHelper.ts" />
module Evolution.DataTypes {

    export class Vector {

        constructor(
            public x: number = 0,
            public y: number = 0) {
        }

        public Clone() {
            return new Vector(this.x, this.y);
        }

        public Invert() {
            this.x *= -1;
            this.y *= -1;
            return this;
        }

        public Add(vec: Vector) {
            this.x += vec.x;
            this.y += vec.y;
            return this;
        }

        public Subtract(vec: Vector) {
            this.x -= vec.x;
            this.y -= vec.y;
            return this;
        }

        public Divide(vec: Vector) {
            this.x /= vec.x;
            this.y /= vec.y;
            return this;
        }

        public Multiply(vec: Vector) {
            this.x *= vec.x;
            this.y *= vec.y;
            return this;
        }

        public LengthSquared() {
            return this.x * this.x + this.y * this.y;
        }

        public Length() {
            return Math.sqrt(this.LengthSquared());
        }

        public DistanceSquared(vec: Vector) {
            var dx: number = this.x - vec.x;
            var dy: number = this.y - vec.y;

            return dx * dx + dy * dy;
        }

        public Distance(vec: Vector) {
            return Math.sqrt(this.DistanceSquared(vec));
        }

        public Normalize() {
            var length = this.Length();

            if (length === 0) {
                this.x = 1;
                this.y = 0;
            } else
                this.Divide(new Vector(length, length));

            return this;
        }

        public Lerp(vec: Vector, amount: number) {
            this.x = MathHelper.LinearInterpolation(this.x, vec.x, amount);
            this.y = MathHelper.LinearInterpolation(this.y, vec.y, amount);
        }

        public Dot(vec: Vector) {
            return this.x * vec.x + this.y * vec.y;
        }

        public Cross(vec: Vector) {
            return (this.x * vec.y) - (this.y * vec.x);
        }

        public JumpTowards(vec: Vector, distance: number) {
            var direction: DataTypes.Vector = vec.Clone().Subtract(this).Normalize();
            var step = direction.Multiply(new Vector(distance, distance));
            this.Add(step);
            return this;
        }

        public JumpRandomDistance(distance: number) {
            this.x += MathHelper.RandomRange(-distance, distance, 4);
            this.y += MathHelper.RandomRange(-distance, distance, 4);
            return this;
        }

        public FindClosestNeighbour(vectors: Vector[]) {
            var closest = vectors[0];
            for (var vec in vectors)
                if (vec != this && this.Distance(vec) < this.Distance(closest))
                    closest = vec;

            return closest;
        }
    }
}