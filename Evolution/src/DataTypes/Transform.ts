﻿ module Evolution.DataTypes {
     
     export class Transform {
         public Position: DataTypes.Vector;
         public Rotation: number;
         public Scale: number

         constructor() {
             this.Position = new DataTypes.Vector();
             this.Rotation = 0;
             this.Scale = 1;
         }
     }
 }