﻿module Evolution.DataTypes {

    export class Color {
        
        constructor(
            public r: number,
            public g: number,
            public b: number,
            public a: number) {
        }

        public GetRgba() {
            return "rgba(" + this.r + "," + this.g + "," + this.b + "," + this.a + ")";
        }

        public GetHex() {
            return "#" +
                this.componentToHex(this.r) +
                this.componentToHex(this.g) +
                this.componentToHex(this.b);
        }

        private componentToHex(c: number) {
            var hex = c.toString(16);
            return hex.length == 1 ? "0" + hex : hex;
        }

        public Lerp(color: Color, amount: number) {
            this.r = MathHelper.LinearInterpolation(this.r, color.r, amount);
            this.g = MathHelper.LinearInterpolation(this.g, color.g, amount);
            this.b = MathHelper.LinearInterpolation(this.b, color.b, amount);
            this.a = MathHelper.LinearInterpolation(this.a, color.a, amount);
        }

        static RandomColor() {
            return new Color(
                MathHelper.RandomRange(0, 255, 0),
                MathHelper.RandomRange(0, 255, 0),
                MathHelper.RandomRange(0, 255, 0), 1);
        }
    }
} 