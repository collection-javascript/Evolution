﻿module Evolution {

    export class SimulationObject {

        public Transform: DataTypes.Transform;

        constructor() {
            this.Transform = new DataTypes.Transform();
        }

        public Step() {
            // Override me. :D
            throw new Error('This method is abstract');
        }
    }
}