﻿module Evolution {

    export class Cell {

        public Sides: number;
        public Neighbours: Cell[];

        constructor(sides: number = 3) {
            this.Sides = sides;
            this.Neighbours = [];
        }

        public ConnectToCell(cell: Cell) {
            this.Neighbours[this.Neighbours.length] = cell;
            cell.Neighbours[cell.Neighbours.length] = this;
        }

        public ConnectToRandomCell(cells: Cell[]) {
            this.ConnectToCell(cells[MathHelper.RandomRange(0, cells.length-1, 0)]);
        }

        public GetPosition() {
            
        }
    }
}