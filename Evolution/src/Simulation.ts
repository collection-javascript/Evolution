/// <reference path="Process.ts" />
module Evolution {

    export class Simulation extends Process {

        public Environment: Environment;
        public SimulationObjects: SimulationObject[];

        constructor() {
            super();
            this.Environment = new Environment();
            this.SimulationObjects = [];
        }

        public Step() {
            for (var i=0; i<this.SimulationObjects.length; i++) {
                var object = this.SimulationObjects[i];
                object.Step();
            }
        }

        public AddSimulationObject(object: SimulationObject) {
            this.SimulationObjects.push(object);
            return object;
        }

    }

}