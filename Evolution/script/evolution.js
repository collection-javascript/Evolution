var Evolution;
(function (Evolution) {
    var Cell = (function () {
        function Cell(sides) {
            if (sides === void 0) { sides = 3; }
            this.Sides = sides;
            this.Neighbours = [];
        }
        Cell.prototype.ConnectToCell = function (cell) {
            this.Neighbours[this.Neighbours.length] = cell;
            cell.Neighbours[cell.Neighbours.length] = this;
        };
        Cell.prototype.ConnectToRandomCell = function (cells) {
            this.ConnectToCell(cells[Evolution.MathHelper.RandomRange(0, cells.length - 1, 0)]);
        };
        Cell.prototype.GetPosition = function () {
        };
        return Cell;
    })();
    Evolution.Cell = Cell;
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var DataTypes;
    (function (DataTypes) {
        var Color = (function () {
            function Color(r, g, b, a) {
                this.r = r;
                this.g = g;
                this.b = b;
                this.a = a;
            }
            Color.prototype.GetRgba = function () {
                return "rgba(" + this.r + "," + this.g + "," + this.b + "," + this.a + ")";
            };
            Color.prototype.GetHex = function () {
                return "#" + this.componentToHex(this.r) + this.componentToHex(this.g) + this.componentToHex(this.b);
            };
            Color.prototype.componentToHex = function (c) {
                var hex = c.toString(16);
                return hex.length == 1 ? "0" + hex : hex;
            };
            Color.prototype.Lerp = function (color, amount) {
                this.r = Evolution.MathHelper.LinearInterpolation(this.r, color.r, amount);
                this.g = Evolution.MathHelper.LinearInterpolation(this.g, color.g, amount);
                this.b = Evolution.MathHelper.LinearInterpolation(this.b, color.b, amount);
                this.a = Evolution.MathHelper.LinearInterpolation(this.a, color.a, amount);
            };
            Color.RandomColor = function () {
                return new Color(Evolution.MathHelper.RandomRange(0, 255, 0), Evolution.MathHelper.RandomRange(0, 255, 0), Evolution.MathHelper.RandomRange(0, 255, 0), 1);
            };
            return Color;
        })();
        DataTypes.Color = Color;
    })(DataTypes = Evolution.DataTypes || (Evolution.DataTypes = {}));
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var DataTypes;
    (function (DataTypes) {
        var Transform = (function () {
            function Transform() {
                this.Position = new DataTypes.Vector();
                this.Rotation = 0;
                this.Scale = 1;
            }
            return Transform;
        })();
        DataTypes.Transform = Transform;
    })(DataTypes = Evolution.DataTypes || (Evolution.DataTypes = {}));
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var MathHelper = (function () {
        function MathHelper() {
        }
        MathHelper.RandomRange = function (min, max, decimals) {
            var random = min + Math.random() * (max - min);
            var power = Math.pow(10, decimals);
            return Math.round(random * power) / power;
        };
        MathHelper.LinearInterpolation = function (point1, point2, amount) {
            return point1 + (point2 - point1) * amount;
        };
        return MathHelper;
    })();
    Evolution.MathHelper = MathHelper;
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var DataTypes;
    (function (DataTypes) {
        var Vector = (function () {
            function Vector(x, y) {
                if (x === void 0) { x = 0; }
                if (y === void 0) { y = 0; }
                this.x = x;
                this.y = y;
            }
            Vector.prototype.Clone = function () {
                return new Vector(this.x, this.y);
            };
            Vector.prototype.Invert = function () {
                this.x *= -1;
                this.y *= -1;
                return this;
            };
            Vector.prototype.Add = function (vec) {
                this.x += vec.x;
                this.y += vec.y;
                return this;
            };
            Vector.prototype.Subtract = function (vec) {
                this.x -= vec.x;
                this.y -= vec.y;
                return this;
            };
            Vector.prototype.Divide = function (vec) {
                this.x /= vec.x;
                this.y /= vec.y;
                return this;
            };
            Vector.prototype.Multiply = function (vec) {
                this.x *= vec.x;
                this.y *= vec.y;
                return this;
            };
            Vector.prototype.LengthSquared = function () {
                return this.x * this.x + this.y * this.y;
            };
            Vector.prototype.Length = function () {
                return Math.sqrt(this.LengthSquared());
            };
            Vector.prototype.DistanceSquared = function (vec) {
                var dx = this.x - vec.x;
                var dy = this.y - vec.y;
                return dx * dx + dy * dy;
            };
            Vector.prototype.Distance = function (vec) {
                return Math.sqrt(this.DistanceSquared(vec));
            };
            Vector.prototype.Normalize = function () {
                var length = this.Length();
                if (length === 0) {
                    this.x = 1;
                    this.y = 0;
                }
                else
                    this.Divide(new Vector(length, length));
                return this;
            };
            Vector.prototype.Lerp = function (vec, amount) {
                this.x = Evolution.MathHelper.LinearInterpolation(this.x, vec.x, amount);
                this.y = Evolution.MathHelper.LinearInterpolation(this.y, vec.y, amount);
            };
            Vector.prototype.Dot = function (vec) {
                return this.x * vec.x + this.y * vec.y;
            };
            Vector.prototype.Cross = function (vec) {
                return (this.x * vec.y) - (this.y * vec.x);
            };
            Vector.prototype.JumpTowards = function (vec, distance) {
                var direction = vec.Clone().Subtract(this).Normalize();
                var step = direction.Multiply(new Vector(distance, distance));
                this.Add(step);
                return this;
            };
            Vector.prototype.JumpRandomDistance = function (distance) {
                this.x += Evolution.MathHelper.RandomRange(-distance, distance, 4);
                this.y += Evolution.MathHelper.RandomRange(-distance, distance, 4);
                return this;
            };
            Vector.prototype.FindClosestNeighbour = function (vectors) {
                var closest = vectors[0];
                for (var vec in vectors)
                    if (vec != this && this.Distance(vec) < this.Distance(closest))
                        closest = vec;
                return closest;
            };
            return Vector;
        })();
        DataTypes.Vector = Vector;
    })(DataTypes = Evolution.DataTypes || (Evolution.DataTypes = {}));
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var Environment = (function () {
        function Environment() {
            this.Size = new Evolution.DataTypes.Vector(512, 512);
            this.Color = Evolution.DataTypes.Color.RandomColor();
        }
        return Environment;
    })();
    Evolution.Environment = Environment;
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var Gui;
    (function (Gui) {
        var DomHelper = (function () {
            function DomHelper() {
            }
            DomHelper.AppendElement = function (element, parent) {
                parent.appendChild(element);
            };
            DomHelper.NewWrapperElement = function () {
                var wrapper = document.createElement("div");
                wrapper.className = "wrapper";
                return wrapper;
            };
            DomHelper.NewContainerElement = function () {
                var container = document.createElement("div");
                container.className = "container";
                return container;
            };
            DomHelper.NewButtonElement = function (text, action) {
                var button = document.createElement("button");
                button.addEventListener("click", function () { return action(); });
                button.innerHTML = text;
                return button;
            };
            DomHelper.NewCanvasElement = function (size) {
                var canvas = document.createElement("canvas");
                canvas.width = size.x;
                canvas.height = size.y;
                return canvas;
            };
            return DomHelper;
        })();
        Gui.DomHelper = DomHelper;
    })(Gui = Evolution.Gui || (Evolution.Gui = {}));
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var Gui;
    (function (Gui) {
        var DrawHelper = (function () {
            function DrawHelper() {
            }
            DrawHelper.FillCanvas = function (ctx, canvas, color) {
                ctx.fillStyle = color.GetRgba();
                ctx.fillRect(0, 0, canvas.width, canvas.height);
            };
            DrawHelper.ClearCanvas = function (ctx, canvas) {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
            };
            DrawHelper.DrawNSide = function (ctx, transform, sides, color) {
                ctx.beginPath();
                DrawHelper.DrawPolygon(ctx, transform, sides);
                ctx.fillStyle = color.GetRgba();
                ctx.fill();
                ctx.strokeStyle = new Evolution.DataTypes.Color(255, 255, 255, 1).GetRgba();
                ctx.lineWidth = 4;
                ctx.stroke();
            };
            DrawHelper.DrawPolygon = function (ctx, transform, sides) {
                if (sides < 3)
                    return;
                var a = (Math.PI * 2) / sides;
                ctx.save();
                ctx.translate(transform.Position.x, transform.Position.y);
                ctx.rotate(transform.Rotation);
                ctx.moveTo(transform.Scale, 0);
                for (var i = 1; i < sides; i++) {
                    ctx.lineTo(transform.Scale * Math.cos(a * i), transform.Scale * Math.sin(a * i));
                }
                ctx.closePath();
                ctx.restore();
            };
            return DrawHelper;
        })();
        Gui.DrawHelper = DrawHelper;
    })(Gui = Evolution.Gui || (Evolution.Gui = {}));
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var Process = (function () {
        function Process() {
            this.Frame = 0;
            this.Interval = 25;
        }
        Process.prototype.Start = function () {
            var _this = this;
            this._loop = setTimeout(function () { return _this.Loop(); }, 0);
            this._lastTime = Date.now();
        };
        Process.prototype.Stop = function () {
            clearTimeout(this._loop);
        };
        Process.prototype.Loop = function () {
            var _this = this;
            this.Frame++;
            this._loop = setTimeout(function () { return _this.Loop(); }, this.getDelay());
            this.Step();
        };
        Process.prototype.getDelay = function () {
            var thisTime = Date.now();
            var deltaTime = thisTime - this._lastTime;
            var delay = Math.max(this.Interval - deltaTime, 0);
            this._lastTime = thisTime + delay;
            return delay;
        };
        Process.prototype.Step = function () {
            throw new Error('This method is abstract');
        };
        return Process;
    })();
    Evolution.Process = Process;
})(Evolution || (Evolution = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Evolution;
(function (Evolution) {
    var Gui;
    (function (Gui) {
        var SimulationGui = (function (_super) {
            __extends(SimulationGui, _super);
            function SimulationGui(simulation, canvas) {
                _super.call(this);
                this._simulation = simulation;
                this._canvas = canvas;
                this._context = this._canvas.getContext('2d');
            }
            SimulationGui.prototype.Step = function () {
                Gui.DrawHelper.ClearCanvas(this._context, this._canvas);
                this.drawEnvironment(this._simulation.Environment);
                this.drawSimulationObjects(this._simulation.SimulationObjects);
                this.Frame++;
            };
            SimulationGui.prototype.drawEnvironment = function (environment) {
                Gui.DrawHelper.FillCanvas(this._context, this._canvas, environment.Color);
            };
            SimulationGui.prototype.drawSimulationObjects = function (objects) {
                for (var i = 0; i < objects.length; i++)
                    this.drawSimulationObject(objects[i]);
            };
            SimulationGui.prototype.drawSimulationObject = function (object) {
                Gui.DrawHelper.DrawNSide(this._context, object.Transform, 5, new Evolution.DataTypes.Color(200, 200, 0, 1));
            };
            return SimulationGui;
        })(Evolution.Process);
        Gui.SimulationGui = SimulationGui;
    })(Gui = Evolution.Gui || (Evolution.Gui = {}));
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var Setup = (function () {
        function Setup() {
            this.buildElements();
            this.appendElements();
            this.Reroll();
        }
        Setup.prototype.buildElements = function () {
            var _this = this;
            this._wrapper = Evolution.Gui.DomHelper.NewWrapperElement();
            this._simulationCanvasBox = Evolution.Gui.DomHelper.NewContainerElement();
            this._simulationCanvas = Evolution.Gui.DomHelper.NewCanvasElement(new Evolution.DataTypes.Vector(512, 512));
            this._buttonBox = Evolution.Gui.DomHelper.NewContainerElement();
            this._btnReroll = Evolution.Gui.DomHelper.NewButtonElement("Reroll", function () { return _this.Reroll(); });
            this._btnStopSimulation = Evolution.Gui.DomHelper.NewButtonElement("Start", function () {
                return null;
            });
            this._btnStartSimulation = Evolution.Gui.DomHelper.NewButtonElement("Stop", function () {
                return null;
            });
        };
        Setup.prototype.appendElements = function () {
            Evolution.Gui.DomHelper.AppendElement(this._wrapper, document.body);
            Evolution.Gui.DomHelper.AppendElement(this._simulationCanvasBox, this._wrapper);
            Evolution.Gui.DomHelper.AppendElement(this._simulationCanvas, this._simulationCanvasBox);
            Evolution.Gui.DomHelper.AppendElement(this._buttonBox, this._wrapper);
            Evolution.Gui.DomHelper.AppendElement(this._btnReroll, this._buttonBox);
            Evolution.Gui.DomHelper.AppendElement(this._btnStopSimulation, this._buttonBox);
            Evolution.Gui.DomHelper.AppendElement(this._btnStartSimulation, this._buttonBox);
        };
        Setup.prototype.Reroll = function () {
            if (this.Simulation)
                this.Simulation.Stop();
            if (this.SimulationGui)
                this.SimulationGui.Stop();
            this.Simulation = new Evolution.Simulation();
            this.Simulation.Start();
            this.SimulationGui = new Evolution.Gui.SimulationGui(this.Simulation, this._simulationCanvas);
            this.SimulationGui.Start();
            this.Simulation.AddSimulationObject(new Evolution.Organism());
            this.Simulation.AddSimulationObject(new Evolution.Organism());
            this.Simulation.AddSimulationObject(new Evolution.Organism());
            this.Simulation.AddSimulationObject(new Evolution.Organism());
        };
        return Setup;
    })();
    Evolution.Setup = Setup;
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var SimulationObject = (function () {
        function SimulationObject() {
            this.Transform = new Evolution.DataTypes.Transform();
        }
        SimulationObject.prototype.Step = function () {
            throw new Error('This method is abstract');
        };
        return SimulationObject;
    })();
    Evolution.SimulationObject = SimulationObject;
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var Organism = (function (_super) {
        __extends(Organism, _super);
        function Organism() {
            _super.call(this);
            this.Cells = [];
            this.Transform.Position = new Evolution.DataTypes.Vector(Evolution.MathHelper.RandomRange(0, 512, 0), Evolution.MathHelper.RandomRange(0, 512, 0));
        }
        Organism.prototype.Step = function () {
        };
        Organism.prototype.AddCell = function () {
            var cell = new Evolution.Cell();
            if (this.Cells.length != 0)
                cell.ConnectToRandomCell(this.Cells);
            this.Cells.push(cell);
            return cell;
        };
        return Organism;
    })(Evolution.SimulationObject);
    Evolution.Organism = Organism;
})(Evolution || (Evolution = {}));
var Evolution;
(function (Evolution) {
    var Simulation = (function (_super) {
        __extends(Simulation, _super);
        function Simulation() {
            _super.call(this);
            this.Environment = new Evolution.Environment();
            this.SimulationObjects = [];
        }
        Simulation.prototype.Step = function () {
            for (var i = 0; i < this.SimulationObjects.length; i++) {
                var object = this.SimulationObjects[i];
                object.Step();
            }
        };
        Simulation.prototype.AddSimulationObject = function (object) {
            this.SimulationObjects.push(object);
            return object;
        };
        return Simulation;
    })(Evolution.Process);
    Evolution.Simulation = Simulation;
})(Evolution || (Evolution = {}));
//# sourceMappingURL=evolution.js.map